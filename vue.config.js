module.exports = {
  lintOnSave: false,
  css: {
    loaderOptions: {
      sass: {
        sourceMap: true,
        // eslint-disable-next-line global-requires
        implementation: require('sass'),
        data: '@import "@/assets/css/_variable.scss";'
      },
    },
  },
};
