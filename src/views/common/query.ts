export interface InventoryQueryData {
  id: string;
  lastSeenTime: string;
  status: string;
  location: {
    name: string;
  };
  itemType: {
    name: string;
  };
}
