import Vue from 'vue';
import Router from 'vue-router';
import { store } from './store';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/HomePage.vue'),
    },

    {
      path: '/location',
      name: 'location',
      component: () => import('./views/location/ListPage.vue'),
    },

    {
      props: true,
      path: '/location/:location',
      name: 'location-detail',
      component: () => import('./views/location/DetailPage.vue'),
    },

    {
      props: true,
      path: '/location/:location/edit',
      name: 'location-edit',
      component: () => import('./views/location/EditPage.vue'),
    },

    {
      path: '/itemType',
      name: 'itemType',
      component: () => import('./views/itemType/ListPage.vue'),
    },

    {
      path: '/itemType/create',
      name: 'itemType-create',
      component: () => import('./views/itemType/CreatePage.vue'),
    },

    {
      props: true,
      path: '/itemType/:itemType',
      name: 'itemType-detail',
      component: () => import('./views/itemType/DetailPage.vue'),
    },

    {
      props: true,
      path: '/itemType/:itemType/edit',
      name: 'itemType-edit',
      component: () => import('./views/itemType/EditPage.vue'),
    },

    {
      path: '/inventory/create',
      name: 'inventory-create',
      component: () => import('./views/inventory/CreatePage.vue'),
    },

    {
      path: '/inventory/batch',
      name: 'inventory-batch',
      component: () => import('./views/inventory/BatchPage.vue'),
    },

    {
      path: '/user',
      name: 'user',
      component: () => import('./views/user/ListPage.vue'),
    },

    {
      path: '/user/create',
      name: 'user-create',
      component: () => import('./views/user/CreatePage.vue'),
    },

    {
      path: '/user/:user',
      name: 'user-detail',
      component: () => import('./views/user/DetailPage.vue'),
      props: true,
    },

    {
      path: '/user/:user/edit',
      name: 'user-edit',
      component: () => import('./views/user/EditPage.vue'),
      props: true,
    },

    {
      path: '/auth',
      name: 'auth',
      component: () => import('./views/AuthPage.vue'),
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.name === 'auth') {
    next();
    return;
  }

  if (store.state.username === null || store.state.password === null) {
    next({ name: 'auth', replace: true });
  } else {
    next();
  }
});

export default router;
