import Repository from '../repository';
import User from './modal';
import { restfulClient } from '../../client';

interface UserInput {
  name: string;
}

const repository: Repository<User, UserInput> = {
  list() {
    return restfulClient.get('user')
      .then(({ data: { data } }) => data as User[]);
  },
  get(id: string) {
    return restfulClient.get(`user/${id}`)
      .then(({ data: { data } }) => data as User);
  },
  create(input: UserInput) {
    return restfulClient.post('user', input)
      .then(({ data: { data } }) => data as User);
  },
  update(id: string, input: UserInput): Promise<User> {
    return restfulClient.patch(`user/${id}`, input)
      .then(({ data: { data } }) => data as User);
  },
  delete(id: string): Promise<any> {
    return restfulClient.delete(`user/${id}`);
  },
};

export default repository;
