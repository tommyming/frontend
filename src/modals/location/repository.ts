import { restfulClient as client } from '@/client';
import Location from './modal';
import Repository from '../repository';

interface LocationInput {
  name: string;
}

const repository: Repository<Location, LocationInput> = {
  list() {
    return client.get('location')
      .then(({ data: { data } }) => (data as Location[]));
  },
  get(id: string) {
    return client.get(`location/${id}`)
      .then(({ data: { data } }) => (data as Location));
  },
  create(input: LocationInput) {
    return client.post('location', input)
      .then(({ data: { data } }) => (data as Location));
  },
  update(id: string, input: LocationInput) {
    return client.patch(`location/${id}`, input)
      .then(({ data: { data } }) => (data as Location));
  },
  delete(id: string) {
    return client.delete(`location/${id}`);
  },
};

export default repository;
