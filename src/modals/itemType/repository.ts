import { restfulClient as client } from '@/client';
import ItemType from './modal';
import Repository from '../repository';

interface ItemTypeInput {
  name: string;
  description: string;
}

const repository: Repository<ItemType, ItemTypeInput> = {
  list() {
    return client.get('itemType')
      .then(({ data: { data } }) => (data as ItemType[]));
  },
  get(id: string) {
    return client.get(`itemType/${id}`)
      .then(({ data: { data } }) => (data as ItemType));
  },
  create(input: ItemTypeInput) {
    return client.post('itemType', input)
      .then(({ data: { data } }) => (data as ItemType));
  },
  update(id: string, input: ItemTypeInput) {
    return client.patch(`itemType/${id}`, input)
      .then(({ data: { data } }) => (data as ItemType));
  },
  delete(id: string) {
    return client.delete(`itemType/${id}`);
  },
};

export default repository;
