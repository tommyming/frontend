export default interface Repository<T, Input> {
  list(): Promise<T[]>;
  get(id: string): Promise<T>;
  create(input: Input): Promise<T>;
  update(id: string, input: Input): Promise<T>;
  delete(id: string): Promise<any>;
}
